const express = require('express');
const app = express();
const path = require('path');
const cookieParser = require('cookie-parser');
const session = require('express-session');


// Enable Request Cookies
app.use(cookieParser());

// Enable Server Sessions
app.use(session({
    secret: 'a912jasd-12398asdh=',
    resave: false,
    saveUninitialized: false,
    cookie: {
        secure: false, // Allows session to work on HTTP
        httpOnly: true, // Disables this cookie for document.cookie
        maxAge: 3600000 // 1 Hour
    }
}));

// Serve my static content (js, css and images);
app.use('/public', express.static( path.join( __dirname, 'www' ) ));

app.get('/', (req, res) => {
    console.log(req.session);
    if (req.session.counter == undefined) {
        res.redirect('/login');
    } else {
        req.session.counter += 1;
        res.redirect('/dashboard')
    }    
});

app.get('/create', (req, res)=>{
    req.session.counter = 0;
    res.send(' Session was created...');
});

app.get('/login', (req, res)=>{
    res.send(' Please Login... ');
});

app.get('/dashboard', (req, res)=>{
    res.send(' This is Dashboard... ');
});

app.listen(3000, () => console.log('App started on Port 3000...'));
